package ZhangHeng.com.shengXuLianbiao;


    class MyNode{
        int data;
        MyNode next;

        public MyNode(){
            this.next=null;
        }
        public MyNode(int e){
            this.data=e;
            this.next=null;
        }

    //插入节点
    public void insert(MyNode head,int e){
        MyNode newNode = new MyNode(e);
        newNode.next=head.next;
        head.next=newNode;
    }

    //链表的大小
        public int size(MyNode head){
            MyNode temp=head;
            int size=0;
            while(temp.next!=null){
                size++;
                temp=temp.next;
            }
            return size;
        }

    //遍历节点
    public void print(MyNode head){
        MyNode temp=head;
        while(temp.next!=null){
            System.out.print(" "+temp.next.data);
            temp=temp.next;
        }
    }
    //get()方法实现
        public int get(MyNode head,int i){
            MyNode temp=head;
              for(int j=0;j<i;j++){
                  temp=temp.next;
              }
              return temp.data;
        }

        //更改数据
        public void changeN(MyNode head,int e,int index){
            for(int i=0;i<index;i++){
                head=head.next;
            }
            head.data=e;
        }

    //排序算法
        public void sort(MyNode head){
            for(int i=1;i<head.size(head);i++){
                for(int j=i+1;j<head.size(head)+1;j++){
                    if(head.get(head,i)>head.get(head,j)){
                        int temp=  head.get(head,j);//获取小值
                        int n =head.get(head,i);
                        head.changeN(head,n,j);
                        head.changeN(head,temp,i);
                    }
                }
            }
        }
}
public class oneUp {
    public static void main(String []args){
        MyNode head = new MyNode();

       int arr0[]={1,2,8,4};
       int arr1[]={6,3,9,7};
       for(int i=0;i<arr0.length;i++){
           head.insert(head,arr0[i]);
       }
       for(int j=0;j<arr1.length;j++){
           head.insert(head,arr1[j]);
       }
        head.print(head);
        head.sort(head);
        System.out.println();
        System.out.println("排序后：");
        head.print(head);

    }
}

package ZhangHeng.com.leetcode34;
class Solution {
    public int[] searchRange(int[] nums, int target) {
        int l = 0;
        int r = nums.length - 1;
        int arr[] = new int[2];
        arr[0] = -1;
        arr[1] = -1;

        int mid ;

        arr[0] = getLeft(nums,target);
        arr[1] = getRight(nums,target);
        return arr;
    }

    public int getLeft(int[] nums,int target){
        int l = 0;
        int r = nums.length-1;
        int mid;
        while(l<r){
            mid = (l+r)/2;
            if(nums[mid]>=target) r = mid;
            if(nums[mid]<target) l = mid + 1;
            if(nums[mid] == target && mid != 0 && nums[mid-1]!=target){
                return mid;
            }
            if(nums[mid] == target && mid == 0)  return mid;
        }
        return -1;
    }


    public int getRight(int[] nums,int target){
        int l = 0;
        int r = nums.length-1;
        int mid;
        while(l<r){
            mid = (l+r)/2+1;
            if(nums[mid]>target) r = mid - 1;
            if(nums[mid]<target) l = mid;
            if(nums[mid] == target && mid != nums.length-1 && nums[mid+1]!=target){
                return mid;
            }
            if(nums[mid] == target && mid == nums.length-1)  return mid;
        }
        return -1;
    }
}

/**
 * 自己写的代码   if  if  太多了，没有逻辑
        */
public class t {
}

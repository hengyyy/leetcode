package ZhangHeng.com.yangHuiTriangle;

import java.util.ArrayList;
import java.util.List;

class Solution {
    public static List<List<Integer>> generate(int numRows) {
        List<List<Integer>> yhTriangle = new ArrayList<>();
        // 至少要输入1行
        if (numRows >= 1) {
            // 初始化第一行
            List<Integer> yh = new ArrayList<>();
            // [1]
            yh.add(1);
            // [[1]]
            yhTriangle.add(yh);
            // 只有一行，则直接返回初始list
            if (numRows == 1) {
                return yhTriangle;
            }
            // 从第二行开始循环
            for (int i = 1; i < numRows; i ++) {
                // 每一行第一个数是1
                yh = new ArrayList<>();
                yh.add(1);
                // 索引从1开始（每行的第二个数），索引下标小于行号i
                for (int j = 1; j < i; j ++) {
                    int count = yhTriangle.get(i - 1).get(j - 1) + yhTriangle.get(i - 1).get(j);
                    yh.add(count);
                }
                // 每一行最后一个数是1
                yh.add(1);
                yhTriangle.add(yh);
            }
        }
        return yhTriangle;
    }
}


public class YangHuiTriangle {
    public static void main(String []args){
        Solution n1 = new Solution();

                System.out.println(Solution.generate(4));


    }
}
